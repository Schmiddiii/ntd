#!/bin/sh
if [[ $1 == "--help" ]];
then
    echo "@movie Store a movie you have watched.
    This will store the movie as a @note and in a extra file.
    Needs: -
    Provides: -
    "
    exit
fi

stdin=$(cat <&0)
mkdir -p $NTD_DATA_DIR
movies=$NTD_DATA_DIR/movies.txt
touch $movies
echo $stdin >> $movies
sort -o $movies $movies
echo "$(uniq $movies)" > $movies
echo -n "@note Watched the #movie : "
echo -n $stdin
