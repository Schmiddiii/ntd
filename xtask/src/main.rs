use std::env;
use std::path::{Path, PathBuf};
use std::process::{Command, Stdio};

fn main() -> Result<(), Box<dyn std::error::Error>> {
    let task = env::args().nth(1);
    match task.as_ref().map(|it| it.as_str()) {
        Some("inst") => inst()?,
        _ => print_help(),
    }
    Ok(())
}

fn print_help() {
    eprintln!(
        "Tasks:
inst            Installs the application and bots.
                Installation directory for binary: ~/.local/bin
                Installation directory for bots: ~/.local/share/ntd/bots
"
    )
}

fn inst() -> Result<(), Box<dyn std::error::Error>> {
    build()?;
    inst_bin()?;
    inst_bots()?;
    Ok(())
}

fn inst_bots() -> Result<(), Box<dyn std::error::Error>> {
    eprintln!("Installing bots");
    let prog_dir = env::var("NTD_BOT_DIR").unwrap_or(
        env::var("XDG_DATA_DIR").map(|s| s + "/ntd/bots").unwrap_or(
            env::var("HOME")
                .map(|s| s + "/.local/share/ntd/bots")
                .map_err(|_| "No directory for the bots found.")?,
        ),
    );

    std::fs::create_dir_all(&prog_dir)?;

    let from = project_root().join("bots");

    for file in from.read_dir()? {
        let file = file?;
        eprintln!("Installing bot: {}", file.file_name().to_string_lossy());
        std::fs::copy(
            &from.join(file.file_name()),
            Path::new(&prog_dir).join(file.file_name()),
        )?;
    }

    Ok(())
}

fn inst_bin() -> Result<(), Box<dyn std::error::Error>> {
    eprintln!("Installing binary");
    let prog_dir = env::var("HOME")
        .map(|s| s + "/.local/bin")
        .map_err(|_| "No directory for the bin found.")?;

    std::fs::create_dir_all(&prog_dir)?;

    let from = project_root().join("target/release/ntd");

    std::fs::copy(
        &from,
        Path::new(&prog_dir).join(from.file_name().expect("File to have name")),
    )?;

    Ok(())
}

fn build() -> Result<(), Box<dyn std::error::Error>> {
    eprintln!("Building binary");
    let cargo = env::var("CARGO").unwrap_or_else(|_| "cargo".to_string());
    let status = Command::new(cargo)
        .current_dir(project_root())
        .args(&["build", "--release"])
        .status()?;

    if !status.success() {
        Err("cargo build failed")?;
    }

    let dst = project_root().join("target/release/ntd");

    if Command::new("strip")
        .arg("--version")
        .stdout(Stdio::null())
        .status()
        .is_ok()
    {
        eprintln!("stripping the binary");
        let status = Command::new("strip").arg(&dst).status()?;
        if !status.success() {
            Err("strip failed")?;
        }
    } else {
        eprintln!("no `strip` utility found")
    }

    Ok(())
}

fn project_root() -> PathBuf {
    Path::new(&env!("CARGO_MANIFEST_DIR"))
        .ancestors()
        .nth(1)
        .unwrap()
        .to_path_buf()
}
