# ntd (speak: noted)

Programmable CLI note taking (and similar) application.

## Idea

This project aims to be a very simple, yet extremely customizable and powerful note-taking, journaling and whatever-you-can-think-of application. For this, one can the application utilizes bots, programs that can be written in any programming language and that can be created with ease.

## Default features

- Store notes.
- List notes for a given day.
- Print a note for a given day with id.
- Add tags to notes (#).
- Print notes by tags.

## Installation

Run `cargo xtask inst` in the repository. This will install the application to `$HOME/.local/bin` (make sure it is in the path, or just move the application afterwards) and the bots to `$HOME/.local/share/ntd/bots`.

## Basic concepts

On its core, the application is basically similar to `echo`. It does not even store what you have written anywhere:

```
> ntd Hello World
Hello World
```

This is where bots come in. Bots take as arguments what is written behind them, interpret the arguments and return what should be stay in the query. Bots are accessed by writing `@` in front of them. The `@note`-bot for example takes its arguments, stores it in a file and remove its arguments from the query:

```
> ntd @note Hello World

> cat ~/.local/share/ntd/data/<date>/note_<id>.txt
Hello World
```

Of course you would not want to cat that file manually, that is where the `@list-notes` comes in. It optionally takes a argument for the date you want to list the notes of and prints the first line of all saved notes:

```
❯ ntd @list-notes
0: Hello World
```

Does not sound that special yet? You can combine bots. Want to take a note of a list of all notes of today (I have no idea why someone would want to do that, but it should show the idea):

```
> ntd @note @list-notes

> ntd @list-notes
0: Hello World
1: 0: Hello World
```

Note that bots will be called last bot first. If you are still not convinced yet, consider the `@note` bot. If you uncover what it really is, is is basically a replacement for `@no-echo @tag @write @set-note-file`. You do not have to know what every one of these bots mean (if the meaning is not clear yet). That is why the default bots also have the possibility to show help information:

```
> ntd --help
A easily extendable note taking application.
Usage:
    ntd [notes]
Specify bots using '@'. These bots will be called last bot first.
The help information of the currently installed bots:

@list List files in a folder relative to the data directory
    The folder must be the first word the bot receives.
    Needs: -
    Provides: -
<...>
```

Notice the `Needs` and `Provides`-section? Bots can share data using a file, where they can write and read to (preferably a file in `.env`-format, but that can be customized if you want to). This is for example the case for the `@write` bot, that receives where it should write to using `NTD_WRITE_FILE`-variable, set for example by the `@set-note-file` bot.

## Configuration

The application, including the bots, are configured using `~/.config/ntd/config.env`. The location of this file can be changed using the environmental variable `NTD_CONFIG_FILE`. The file should at least contain the variables `NTD_DATA_DIR`, which is by default initialized to `$HOME/.local/share/ntd/data`. It can also contain `NTD_BOT_DIR`, which is `$HOME/.local/share/ntd/bots` is not changed. One can also set any other environmental variables in the configuration, they will be passed down to the bots.

## Bot internals

As previously stated, bots are very simple programs. They receive their arguments in the standard input and give their output just using the standard output. As a example, look at the `@write` bot:

```sh
#!/bin/sh

if [[ $1 == "--help" ]];
then
    echo "@write Write everything after the call to a file.
    The file has to be given by NTD_WRITE_FILE (relative to NTD_DATA_DIR) by other bots.
    Needs: NTD_WRITE_FILE
    Provides: -
    "
    exit
fi

source $NTD_SHARED_FILE
out=$NTD_DATA_DIR/$NTD_WRITE_FILE

stdin=$(cat <&0)
echo -n "$stdin" >> "$out"
echo -n "$stdin"
```

The first few lines are basically just help information. The `source`-line ready the shared file. The location of the shared file is given using the environmental variable `NTD_SHARED_FILE`. Afterwards, the output location for the file is computed, the standard input is redirected to the file and standard output.

That is basically everything needed to write a simple bot. But bots can get as complex or simple as needed. Currently the most complex bot is probably `@tag` (but the idea of the bots execution is also pretty simple), just take a look at the source code if you want.

Note that currently all default bots are shell programs. But the bots can be of any programming language (and can have arbitrary file endings), they just have to be executable.

## Contribute

If you wrote a bot you think is worth sharing, just open a MR or similar for inclusion in the `bots-examples` directory.
