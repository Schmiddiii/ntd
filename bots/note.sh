#!/bin/sh
if [[ $1 == "--help" ]];
then
    echo "@note Alias for '@no-echo @tag @write @set-note-file'
    Needs: -
    Provides: -
    "
    exit
fi
echo -n "@no-echo @tag @write @set-note-file "
cat <&0
