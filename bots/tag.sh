#!/bin/sh

if [[ $1 == "--help" ]];
then
    echo "@tag Saves a reference to a note using tags.
    Tags are given using '#'.
    Needs: NTD_WRITE_FILE
    Provides: -
    "
    exit
fi

source $NTD_SHARED_FILE

stdin=$(cat <&0)
echo -n "$stdin"
mkdir -p $NTD_DATA_DIR/tags

for word in $stdin;
do
    if [[ $word == \#* ]];
    then
        word_without_hashtag=$(echo $word | sed 's/#//')
        echo $NTD_WRITE_FILE >> $NTD_DATA_DIR/tags/$word_without_hashtag.txt
    fi
done
