#!/bin/sh

if [[ $1 == "--help" ]];
then
    echo "@cat-note Print the contents of a note.
    This script will try to receive two arguments, the first one is the note date, if not present today will be taken.
    The second argument is the note id, given by @list-notes. This one is mandatory.
    Needs: -
    Provides: -
    "
    exit
fi

stdin=$(cat <&0)
first_arg=$(echo "$stdin" | head -n 1 | awk '{print $1;}')
second_arg=$(echo "$stdin" | head -n 1 | awk '{print $2;}')
if [[ $first_arg =~ ^[0-9]{4}-[0-9]{2}-[0-9]{2}$ ]];
then
    date=$first_arg
    note=$second_arg
    to_cut=3
else
    date=$(date +%F)
    note=$first_arg
    to_cut=2
fi
res=$(cat $NTD_DATA_DIR/$date/note_$note.txt)
echo -n "$res "
echo -n "$stdin" | cut -s -d' ' -f$to_cut- -
