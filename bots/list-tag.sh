#!/bin/sh

if [[ $1 == "--help" ]];
then
    echo "@list-tag List notes by tag.
    The tag (without '#') must be the first word the bot receives.
    Needs: -
    Provides: -
    "
    exit
fi

mkdir -p $NTD_DATA_DIR/tags

stdin=$(cat <&0)
first=$(echo -n "$stdin" | head -n 1 | awk '{print $1;}')
ls_res=$(cat "$NTD_DATA_DIR/tags/$first.txt" | xargs -I % sh -c "echo -n '%: ' | sed 's|note_||;s|.txt||;s|/| |'; head -n 1 $NTD_DATA_DIR/%; echo ''")
echo -n "$ls_res"
echo -n "$stdin" | head -n 1 | cut -s -d' ' -f2- -
