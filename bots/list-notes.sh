#!/bin/sh

if [[ $1 == "--help" ]];
then
    echo "@list-notes List notes of a given date or today.
    Needs: -
    Provides: -
    "
    exit
fi

stdin=$(cat <&0)
if [[ -z $stdin ]];
then
    date=$(date +%F)
else
    date=$(echo "$stdin" | head -n 1 | awk '{print $1;}')
fi
ls_res=$(ls -v "$NTD_DATA_DIR/$date" | xargs -I % sh -c "echo -n '%: ' | sed 's/note_//;s/.txt//'; head -n 1 $NTD_DATA_DIR/$date/%; echo ''")
echo "$ls_res"
echo "$stdin" | cut -s -d' ' -f2- -
