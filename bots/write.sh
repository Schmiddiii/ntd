#!/bin/sh

if [[ $1 == "--help" ]];
then
    echo "@write Write everything after the call to a file.
    The file has to be given by NTD_WRITE_FILE (relative to NTD_DATA_DIR) by other bots.
    Needs: NTD_WRITE_FILE
    Provides: -
    "
    exit
fi

source $NTD_SHARED_FILE
out=$NTD_DATA_DIR/$NTD_WRITE_FILE

stdin=$(cat <&0)
echo -n "$stdin" >> "$out"
echo -n "$stdin"
