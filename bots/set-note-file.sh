#!/bin/sh

if [[ $1 == "--help" ]];
then
    echo "@set-note-file Set the file to write to for notes.
    The files will be stored under a folder representing the current day and numbered sequentially.
    Needs:  -
    Provides: NTD_WRITE_FILE
    "
    exit
fi

today=$(date +%F)
mkdir -p $NTD_DATA_DIR/$today
num=$(ls $NTD_DATA_DIR/$today 2> /dev/null | wc -l)
echo NTD_WRITE_FILE=$today/note_$num.txt >> $NTD_SHARED_FILE

cat <&0
