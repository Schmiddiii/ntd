#!/bin/sh

if [[ $1 == "--help" ]];
then
    echo "@list List files in a folder relative to the data directory
    The folder must be the first word the bot receives.
    Needs: -
    Provides: -
    "
    exit
fi

stdin=$(cat <&0)
first=$(echo "$stdin" | head -n 1 | awk '{print $1;}')
ls_res=$(ls -v "$NTD_DATA_DIR/$first")
echo "$ls_res"
echo "$stdin" | cut -s -d' ' -f2- -
