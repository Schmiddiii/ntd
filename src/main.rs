mod app;
mod bot;
mod bots;
mod config;
mod error;
mod util;

use std::env;

use error::Error;

use crate::{app::App, config::Config};

fn main() -> Result<(), Error> {
    env_logger::init();
    let config = Config::new()?;
    log::debug!("Configuration: {:?}", config.env());
    config.apply();
    let app = App::new()?;
    let result = if env::args().any(|s| s == "--help" || s == "-h") {
        app.help()?
    } else {
        app.run()?
    };

    println!("{}", result);
    Ok(())
}
