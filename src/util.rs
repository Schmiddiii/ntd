use std::{env, fs::OpenOptions, path::PathBuf};

use crate::error::Error;

pub fn create_tempfile() -> Result<PathBuf, Error> {
    create_tempfile_with_name("ntd.env")
}

pub fn create_tempfile_with_name<S: AsRef<str>>(name: S) -> Result<PathBuf, Error> {
    let mut tempfile = env::temp_dir();
    tempfile.push(name.as_ref());
    log::debug!("Use temp file {}", tempfile.to_string_lossy());
    // Create tempfile
    let _open = OpenOptions::new()
        .write(true)
        .create(true)
        .truncate(true)
        .open(&tempfile)?;
    Ok(tempfile)
}
