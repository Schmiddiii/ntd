use std::{
    env,
    fs::{self, OpenOptions},
    io::{Read, Write},
    path::PathBuf,
};

use crate::error::Error;

const DEFAULT_CONFIG: &str = r#"NTD_DATA_DIR=$HOME/.local/share/ntd/data"#;

pub struct Config {
    envs: Vec<(String, String)>,
}

impl Config {
    pub fn new() -> Result<Self, Error> {
        let conf_file = PathBuf::from(
            env::var("NTD_CONFIG_FILE").unwrap_or(
                env::var("XDG_CONFIG_HOME")
                    .map(|s| s + "/ntd/config.env")
                    .unwrap_or(
                        env::var("HOME")
                            .map(|s| s + "/.config/ntd/config.env")
                            .map_err(|_| Error::NoConfigFile)?,
                    ),
            ),
        );

        log::debug!("Reading configuration at {}", conf_file.to_string_lossy());

        if !conf_file.exists() {
            log::debug!("Configuration does not yet exist. Overwriting with default");
            let mut conf_dir = conf_file.clone();
            conf_dir.pop();
            fs::create_dir_all(conf_dir)?;

            let mut file = OpenOptions::new()
                .write(true)
                .create(true)
                .open(conf_file)?;
            let _ = write!(file, "{}", DEFAULT_CONFIG);
            let envs = parse_dotenv(DEFAULT_CONFIG)?;
            Ok(Self { envs })
        } else {
            let mut file = OpenOptions::new().read(true).open(conf_file)?;
            let mut content = String::new();
            file.read_to_string(&mut content)?;
            let envs = parse_dotenv(content)?;
            Ok(Self { envs })
        }
    }

    pub fn env(&self) -> Vec<(String, String)> {
        self.envs.clone()
    }

    pub fn apply(&self) {
        self.envs.iter().for_each(|(k, v)| env::set_var(k, v));
    }
}

// TODO: All dotenv support.
fn parse_dotenv<S: AsRef<str>>(s: S) -> Result<Vec<(String, String)>, Error> {
    let content = s.as_ref().to_owned();
    content
        .lines()
        .map(|s| {
            let mut split = s.split('=');
            let key = split.next().ok_or(Error::ConfigInvalid)?;
            let value = split.next().ok_or(Error::ConfigInvalid)?;
            Ok((key.to_string(), substitute_envs(value)?))
        })
        .collect::<Result<Vec<(String, String)>, Error>>()
}

fn substitute_envs<S: AsRef<str>>(s: S) -> Result<String, Error> {
    let mut s = s.as_ref().to_owned();
    while let Some(env) = extract_env_variable(&s) {
        log::debug!("Replace env variable in configuration: {}", env);
        s = s.replace(
            &format!("${}", env),
            &env::var(env).map_err(|_| Error::ConfigInvalid)?,
        );
    }

    Ok(s)
}

fn extract_env_variable<S: AsRef<str>>(s: S) -> Option<String> {
    let string = s.as_ref().to_owned();
    let mut iter = string.chars().skip_while(|c| c != &'$');

    let mut var = String::new();

    iter.next()?;

    for next in iter {
        if next.is_alphanumeric() {
            var.push(next)
        } else {
            return Some(var);
        }
    }

    if var.is_empty() {
        None
    } else {
        Some(var)
    }
}
