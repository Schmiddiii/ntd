use std::fmt::Display;

#[derive(Debug)]
pub enum Error {
    IO(std::io::Error),
    BotNotFound(String),
    NoBotDir,
    NoConfigFile,
    ConfigInvalid,
}

impl Display for Error {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            Error::IO(e) => write!(f, "IO-Error: {}.", e),
            Error::BotNotFound(bot) => write!(f, "No bot found with name {}.", bot),
            Error::NoBotDir => write!(f, "No directory for the bots found."),
            Error::NoConfigFile => write!(f, "No config file can be found."),
            Error::ConfigInvalid => write!(f, "The configuration is invalid."),
        }
    }
}

impl From<std::io::Error> for Error {
    fn from(e: std::io::Error) -> Self {
        Error::IO(e)
    }
}
