use std::{env, fs, path::PathBuf};

use crate::{bots::Bots, error::Error};

const BASE_HELP: &str = "A easily extendable note taking application.
Usage:
    ntd [arguments]
Specify bots using '@'. These bots will be called last bot first.
The help information of the currently installed bots:

";

pub struct App {
    bots: Bots,
    tempfile: PathBuf,
}

impl Drop for App {
    fn drop(&mut self) {
        let _ = fs::remove_file(&self.tempfile);
    }
}

impl App {
    pub fn new() -> Result<Self, Error> {
        let bots = Bots::default()?;
        let tempfile = crate::util::create_tempfile()?;
        env::set_var("NTD_SHARED_FILE", tempfile.as_os_str());

        Ok(Self { bots, tempfile })
    }

    pub fn run(&self) -> Result<String, Error> {
        let args = env::args().skip(1).collect::<Vec<String>>().join(" ");

        log::debug!("Got args: {}", args);

        let mut input_str = args;

        while let Some(bot) = extract_bot(&input_str) {
            log::debug!("Request for bot execution: {}", bot);
            let position_last_bot = position_last_bot(&input_str)
                .expect("There should be a last @ if a bot could be extracted.");
            let mut start_after_bot = position_last_bot + bot.chars().count() + 1;
            if input_str.chars().nth(start_after_bot).is_some() {
                start_after_bot += 1;
            }
            let arg = input_str.chars().skip(start_after_bot).collect::<String>();
            input_str = input_str.chars().take(position_last_bot).collect();
            log::debug!("Argument for bot: {}", arg);
            let bot_call = self.bots.bot(bot)?;
            let out_str = bot_call.execute(&arg)?;
            log::debug!("Successfull bot execution with output: {}", &out_str);
            input_str += &out_str;
        }

        Ok(input_str)
    }

    pub fn help(&self) -> Result<String, Error> {
        log::debug!("Got request to print help");
        let mut help = BASE_HELP.to_owned();
        let bots = self.bots.all_bots();

        for bot in bots {
            help += &bot.help()?;
        }

        Ok(help)
    }
}

fn extract_bot<S: AsRef<str>>(s: S) -> Option<String> {
    let string = s.as_ref();
    let iter = string.split_whitespace().rev();
    let mut iter = iter.skip_while(|w| !w.starts_with('@'));

    iter.next().map(|s| {
        let mut word = s.to_owned();
        word.remove(0);
        word
    })
}

fn position_last_bot<S: AsRef<str>>(s: S) -> Option<usize> {
    let string = s.as_ref();
    let mut iter = string.chars().rev();
    let last_at = iter.position(|c| c == '@');
    last_at.map(|l| string.chars().count() - l - 1)
}
