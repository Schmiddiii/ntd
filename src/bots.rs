use std::{collections::HashMap, env, path::Path};

use crate::{
    bot::{Bot, CommandBot, EditBot},
    error::Error,
};

pub struct Bots {
    bots: HashMap<String, Box<dyn Bot>>,
}

impl Bots {
    pub fn new<P: AsRef<Path>>(path: P) -> Result<Self, Error> {
        let path = path.as_ref().to_path_buf();

        // TODO: Symlinks
        if !path.is_dir() {
            Err(Error::NoBotDir)
        } else {
            let command_bots = path
                .read_dir()?
                .filter(|e| e.is_ok())
                .map(|e| e.unwrap())
                .filter(|e| e.file_type().map(|t| t.is_file()).unwrap_or_default())
                .map(|f| CommandBot::new(f.path()));

            let mut bots = HashMap::new();

            command_bots.for_each(|b| {
                let _ = bots.insert(b.name(), Box::new(b) as Box<dyn Bot>);
            });

            let edit = EditBot::new()?;
            bots.insert(edit.name(), Box::new(edit));
            Ok(Self { bots })
        }
    }

    pub fn default() -> Result<Self, Error> {
        let prog_dir = env::var("NTD_BOT_DIR").unwrap_or(
            env::var("XDG_DATA_DIR").map(|s| s + "/ntd/bots").unwrap_or(
                env::var("HOME")
                    .map(|s| s + "/.local/share/ntd/bots")
                    .map_err(|_| Error::NoBotDir)?,
            ),
        );

        log::debug!("Use prog dir {}", prog_dir);
        Bots::new(prog_dir)
    }

    pub fn bot<S: AsRef<str>>(&self, name: S) -> Result<&dyn Bot, Error> {
        self.bots
            .get(name.as_ref())
            .ok_or_else(|| Error::BotNotFound(name.as_ref().to_string()))
            .map(|b| &**b)
    }

    pub fn all_bots(&self) -> Vec<&dyn Bot> {
        let mut bots: Vec<&dyn Bot> = self.bots.values().map(|b| &**b).collect();
        bots.sort_by_cached_key(|b| b.name());
        bots
    }
}
